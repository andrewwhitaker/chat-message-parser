/*
 * chat-message-parser
 *
 * Copyright (c) 2015 Andrew Whitaker
 * Licensed under the MIT license.
 */

'use strict';
var MENTION_REGEX = /@([a-z0-9]+)/ig
    , EMOTICON_REGEX = /\(([a-z0-9]{1,15})\)/ig
    , linkHelpers = require('./linkhelpers.js');
    
function getTokens(message, regex) {
  var match
    , results = [];
    
  while ((match = regex.exec(message)) !== null) {
    results.push(match[1]);
  }
  
  return results.length ? results : undefined;
}

function extractMentions(message) {
  return getTokens(message, MENTION_REGEX);
}

function extractEmoticons(message) {
  return getTokens(message, EMOTICON_REGEX);
}

function extractLinks(message, callback) {
  var urls = linkHelpers.extractUrls(message)
    , count
    , linkObjects = [];
  
  if (urls === undefined) {
    callback(message, urls);
    return;
  }
  
  count = urls.length;
  
  urls.forEach(function (url) {
    message = message.replace(url, '');
    linkHelpers.getPageTitle(url, function (title) {
      linkObjects.push({ 
        url: url, 
        title: title
      });
      
      count--;
        
      if (count === 0) {
        callback(message, linkObjects);
      }
    });
  });
}

function validateArguments(message, callback) {
  if (typeof (message) !== 'string') {
    throw new TypeError('message must be a string');
  }
  
  if (typeof (callback) !== 'function') {
    throw new TypeError('callback must be a function');
  }
}

exports.parse = function (message, callback) {  
  validateArguments(message, callback);
  
  message = linkHelpers.removeEmailAddresses(message);

  extractLinks(message, function (updatedMessage, links) {
    var result = {
      links: links,
      mentions: extractMentions(updatedMessage),
      emoticons: extractEmoticons(updatedMessage)
    };
    
    callback(JSON.stringify(result));
  });    
};