# Simple chat message parser

This node module simply accepts a message and extracts various information out of it. The following are supported:

* Mentions, e.g. @Andrew. These are an `@` followed by any number of letters (A-Za-z) or numbers (0-9).
* Emoticons, e.g. (smile). These are 1 to 15 letters (A-Za-z) or numbers (0-9) surrounded by parentheses.
* Links, e.g. http://www.google.com. These are anything that looks like a link: http://google.com, google.com, www.google.com, etc.

For example, if you supplied the `parse` method the following message:
```
"@Andrew's website is http://www.andrew-whitaker.com (smile)"
```
You'd get back the following string:
```
{"mentions":["Andrew"],"emoticons":["smile"],"links":[{"url":"http://www.andrew-whitaker.com","title":"Andrew Whitaker"}]}
```
## Example Usage

Since the message parser attempts to determine the `title` of any URLs in the message, the `parse` method requires a callback to call after it's finished analyzing the message. A simple call would look like:

```javascript
var parser = require('chat-message-parser');

parser.parse(message, function (result) {
    console.log(result);
});
```
## Dependencies

* [request](https://github.com/request/request), for making HTTP requests used to retrieve a URL's associated page title.
* [cheerio](https://www.npmjs.com/package/cheerio) for parsing out the `<title />` element's text
* [nodeunit](https://github.com/caolan/nodeunit) for unit testing
* [Grunt](http://gruntjs.com/) task runner
* [JSHint](http://jshint.com/)

## Overview of technical decisions

The code should be pretty straightforward, but there are a few limitations:

1. *The only type of URLs supported are those referring to web pages*: Since one of the goals of this project is to extract the title of the page corresponding to a URL in a message, `ftp`, `mailto`, etc are not supported. The only supported protocols are `http` and `https`. The regex used for detecting a URL will yield false positives. I decided that in a chat application, you'd rather err on the side of making something into a link than missing things that the user intends to be links. For example, the strictest regular expressions for determining links won't recognize links excluding "http://", but it's very likely that a user would just type "bitbucket.org" and expect it to work.
2. *Decision to parse messages asynchronously:* At first, the `parse` method was synchronous, but given the fact that the `parse` method could be requesting multiple URLs and processing the associated page titles, it seemed more appropriate to accept a callback.

## Building and running tests

1. Clone the repository using `git clone`
2. Use `npm install` to install dependencies.
3. use `grunt` to lint and run tests. The tests may take a little while to run since they make various HTTP requests.
