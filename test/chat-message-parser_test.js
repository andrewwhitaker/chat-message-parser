'use strict';
var parser = require('../lib/chat-message-parser.js')
  , EMPTY_OBJECT = JSON.stringify({})
  , NOOP = function() {};
  
function linkSorter (one, other) {
  if (one.url > other.url) {
    return 1;
  }
  
  if (one.url < other.url) {
    return -1;
  }
  
  return 0;
}

exports['parse validation'] = {
  'invalid data types throw an exception': function (test) {
    var message = 'message must be a string';
    
    test.throws(function () { parser.parse({}, NOOP); }, TypeError, message);
    test.throws(function () { parser.parse([], NOOP); }, TypeError, message);
    test.throws(function () { parser.parse(null, NOOP); }, TypeError, message);
    test.throws(function () { parser.parse(undefined, NOOP); }, TypeError, message);
    test.throws(function () { parser.parse(true, NOOP); }, TypeError, message); 
    test.throws(function () { parser.parse(4, NOOP); }, TypeError, message);
    
    test.done();
  },
  
  'string does not throw an exception': function (test) {
    test.doesNotThrow(function () { parser.parse('', NOOP); });
    test.doesNotThrow(function () { parser.parse('hello world', NOOP); });
    
    test.done();
  }
};

exports['parse mentions'] = {
  'a single mention': function (test) {
    var expected = JSON.stringify({
        mentions: ['chris']
      });
      
    parser.parse('@chris you around?', function (actual) {
      test.strictEqual(actual, expected);
      test.done();
    });      
  },
  
  'multiple mentions': function (test) {
    var expected = JSON.stringify({
        mentions: ['dorothy', 'andrew']
      });
      
    parser.parse('@dorothy, you around? @andrew is looking for you.', function (actual) {
      test.strictEqual(actual, expected);
      test.done();
    });
  },
  
  'a single mention surrounded by punctuation': function (test) {
    var expected = JSON.stringify({
            mentions: ['andrew']
        });
    
    parser.parse('Hey,@andrew: you around?', function (actual) {
      test.strictEqual(actual, expected);
      test.done();
    });
  },
  
  'zero mentions': function (test) {    
    parser.parse('Hey, nobody mentioned here!', function (actual) {
      test.strictEqual(actual, EMPTY_OBJECT);
      test.done();
    });
  },
  
  '@ symbol followed by a non-word character': function (test) {
    parser.parse('Hey, @. Just a test.', function (actual) {
      test.strictEqual(actual, EMPTY_OBJECT);
      test.done();      
    });
  },
  
  'mention with no spaces surrounding it': function (test) {
    var expected = JSON.stringify({
      mentions:['someone']
    });
    
    parser.parse('Thisisarunonsentence@someone,shouldfixit', function (actual) {
      test.strictEqual(actual, expected);
      test.done();
    });
  }
};

exports['parse emoticons'] = {
  'no emoticons': function (test) {
    parser.parse('This message has no emoticons!', function (actual) {
      test.strictEqual(actual, EMPTY_OBJECT);
      test.done();
    });
  },
  
  'a single emoticon': function (test) {
    var expected = JSON.stringify({
        emoticons: ['smile']
      });
        
    parser.parse('Good morning! (smile) how is everyone?', function (actual) {
      test.strictEqual(actual, expected);
      test.done();
    });
  },
  
  'multiple emoticons': function (test) {
    var expected = JSON.stringify({
        emoticons: ['megusta', 'coffee']
      });

    parser.parse('Good morning! (megusta) (coffee)', function (actual) {
      test.strictEqual(actual, expected);
      test.done();      
    });
  },
  
  'emoticon longer than 15 characters': function (test) {
    parser.parse('Hey, this isn\'t an emoticon (abcdefghijklmnop)', function (actual) {
      test.strictEqual(actual, EMPTY_OBJECT);
      test.done();
    });
  },
  
  'emoticon exactly 15 characters long': function (test) {
    var expected = JSON.stringify({
      emoticons: ['abcdefghijklmno']
    });
    
    parser.parse('Hey, this is an emoticon (abcdefghijklmno)', function (actual) {
      test.strictEqual(actual, expected);
      test.done();
    });
  },  
  
  'empty parenthesis': function (test) {
    parser.parse('Testing empty parens () message', function (actual) {    
      test.strictEqual(actual, EMPTY_OBJECT);
      test.done();
    });
  }
};

exports['parse links'] = {
  'a single link': function (test) {
    var expected = JSON.stringify({
      links: [{ url: 'http://www.stackoverflow.com', title: 'Stack Overflow' }]
    });
    
    parser.parse('Hey check this out: http://www.stackoverflow.com', function (actual) {
      test.strictEqual(actual, expected);
      test.done();
    });
  },
  
  'multiple different links': function (test) {
    var expected = {
      links:[
        {
          'url': 'http://about.museum',
          'title': 'Welcome to .museum'
        },      
        {
          'url': 'http://www.google.com',
          'title': 'Google'
        }
      ]
    };
    
    parser.parse('I was searching http://www.google.com and found http://about.museum!', function (actual) {
      var parsed = JSON.parse(actual);

      // links could come back in a different order; we don't really care, we just need to know
      // if they all were found.
      parsed.links.sort(linkSorter);
      
      test.deepEqual(parsed, expected);
      test.done();
    });
  },
  
  'multiple same links': function (test) {
    var expected = JSON.stringify({
      links: [
        {
          'url': 'http://www.theverge.com',
          'title': 'The Verge'
        },
        {
          'url': 'http://www.theverge.com',
          'title': 'The Verge'
        }
      ]
    });
    
    parser.parse('I read some news on http://www.theverge.com that I thought you\'d like. That\'s http://www.theverge.com.', function (actual) {
      test.strictEqual(actual, expected);
      test.done();
    });
  },
  
  'well formed but invalid link': function (test) {
    var expected = {
      links: [{ url: 'http://www.bad-example-doesnt-work.com/', title: '' }]
    };
    
    parser.parse('This looks like it could be valid, but it is not: http://www.bad-example-doesnt-work.com/', function (actual) {
      test.deepEqual(JSON.parse(actual), expected);
      test.done();
    });
  },
  
  'no protocol, but still probably urls': function (test) {
    var expected = {
      links: [{ url: 'google.com', title: 'Google' }]
    };
    
    parser.parse('Did you try searching on google.com?', function (actual) {
      test.deepEqual(JSON.parse(actual), expected);
      test.done();
    });    
  },
  
  'no protocol, but has www': function (test) {
    var expected = JSON.stringify({
      links: [{ url: 'www.vt.edu', title: ' Virginia Tech | Invent the Future | Virginia Tech '}]
    });
    
    parser.parse('Check out www.vt.edu for more information', function (actual) {
      test.deepEqual(actual, expected);
      test.done();
    });
  },
  
  'email addresses are not recognized as links': function (test) {
    parser.parse('Hey, email aawhitaker@gmail.com with questions', function (actual) {
      test.strictEqual(actual, EMPTY_OBJECT);
      test.done();
    });
  },
  
  'links using https://': function (test) {
    var expected = JSON.stringify({
      links: [{
        url: 'https://twitter.com/jdorfman/status/430511497475670016',
        title: 'Justin Dorfman on Twitter: "nice @littlebigdetail from @HipChat (shows hex colors when pasted in chat). http://t.co/7cI6Gjy5pq"' 
      }]
    });
    
    parser.parse('such a cool feature: https://twitter.com/jdorfman/status/430511497475670016', function (actual) {
      test.strictEqual(actual, expected);
      test.done();
    });
  },
  
  'links inside of parenthesis': function (test) {
    var expected = JSON.stringify({
      links: [{
        url: 'http://www.stackoverflow.com',
        title: 'Stack Overflow'
      }]
    });
    
    parser.parse('That site (http://www.stackoverflow.com) is such a useful tool!', function (actual) {
      test.strictEqual(actual, expected);
      test.done();
    });
  },
  
  'links shorter than 15 characters inside of parentheses': function (test) {
    var expected = JSON.stringify({
      links: [{
        url: 'imdb.com',
        title: 'IMDb - Movies, TV and Celebrities - IMDb'
      }]
    });
    
    parser.parse('I love movies (imdb.com)', function (actual) {
      test.strictEqual(actual, expected);
      test.done();
    });
  },
  
  'links with parentheses': function (test) {
    var expected = JSON.stringify({
        links: [{
          url: 'http://en.wikipedia.org/wiki/Saw_(franchise)',
          title: 'Saw (franchise) - Wikipedia, the free encyclopedia'
      }]
    });
    
    parser.parse('Have you seen these terrible movies? http://en.wikipedia.org/wiki/Saw_(franchise)', function (actual) {
      test.strictEqual(actual, expected);
      test.done();
    });
  }
};

exports['links, emoticons and mentions'] = {
  'two mentions and one link': function (test) {
    var expected = JSON.stringify({
      links: [{
        url: 'http://www.stackoverflow.com',
        title: 'Stack Overflow'
      }],      
      mentions: ['jim', 'joe' ]
    });
    
    parser.parse('Hey, @jim, @joe, check this out: http://www.stackoverflow.com', function (actual) {
      test.strictEqual(actual, expected);
      test.done();
    });
  },
  
  'three mentions and two emoticons': function (test) {
    var expected = JSON.stringify({
      mentions: ['Donald', 'Ritchie', 'Michael'],
      emoticons: ['smile', 'frown', 'coffee']
    });
    
    parser.parse('@Donald (smile), @Ritchie (frown), and @Michael went to get some (coffee)', function (actual) {
      test.strictEqual(actual, expected);
      test.done();
    });
  },
  
  'three mentions, two emoticons, ande one URL': function (test) {
    var expected = JSON.stringify({
      links:[{
        url: 'starbucks.com',
        title: '\r\n    Starbucks Coffee Company\r\n'
      }],
      mentions: ['Donald', 'Ritchie', 'Michael'],
      emoticons: ['smile', 'frown', 'coffee']
    });
    parser.parse('@Donald (smile), @Ritchie (frown), and @Michael went to get some (coffee) at starbucks.com', function (actual) {    
      test.strictEqual(actual, expected);
      test.done();
    });
  }  
  
};